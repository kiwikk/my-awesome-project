package com.kiwikk.myawesomeproject.person;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class Person {
    private String _id;
    private String name;
    private String info;
    private String sDate;
    private LocalDate birthDate;
    private int weeks;

    public Person(String name, String date) {
        this.name = name;
        setBirthDate(date);
    }

    public Person() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthDate(String date) {
        sDate = date;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd MM yyyy");
        birthDate = LocalDate.parse(date, dtf);

        setWeeks();
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setWeeks() {
        LocalDate now = LocalDate.now();
        weeks = (int) ChronoUnit.WEEKS.between(birthDate, now);
    }

    public int getWeeks() {
        return weeks;
    }

    public String getName() {
        return name;
    }

    public String getInfo() {
        return info;
    }

    public LocalDate getLDate() {
        return birthDate;
    }

    public String getDate() {
        return sDate;
    }
}
