package com.kiwikk.myawesomeproject.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import com.kiwikk.myawesomeproject.person.Person;

public class DataBaseHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "Person";
    private static final int DB_VERSION = 1;
    private int is = 0;
    private SQLiteDatabase db;
    private static DataBaseHelper singleton;

    public DataBaseHelper(@Nullable Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE PERSON ("
                + "_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                + "NAME TEXT, "
                + "DATE_OF_BIRTH TEXT,"
                + "WEEKS INTEGER);");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public void insertPerson(Person person, SQLiteDatabase db) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAME", person.getName());
        contentValues.put("DATE_OF_BIRTH", person.getDate());
        contentValues.put("WEEKS", person.getWeeks());
        db.insert("PERSON", null, contentValues);
    }

    public void updateDB(Person person, SQLiteDatabase db, String column) {
        ContentValues contentValues = new ContentValues();
        if (column.equals("NAME")) {
            contentValues.put(column, person.getName());
        } else contentValues.put(column, person.getDate());

        db.update("PERSON",
                contentValues,
                "_id=?",
                new String[]{String.valueOf(1)});
    }
}
