package com.kiwikk.myawesomeproject.elements;

import android.content.Context;
import android.graphics.Paint;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import com.kiwikk.myawesomeproject.R;

public class Task extends LinearLayout {
    private String text;
    private boolean isDone;
    private int id;
    private ImageButton deleteButton;
    private View view;
    private CheckBox checkBox;

    public Task(Context context, View view) {
        super(context);
        init(context);
        setSaveEnabled(true);
        this.view = view;
    }


    public Task(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public Task(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public Task(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(Context context) {
        ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.task_layout, this, true);
        setOrientation(LinearLayout.VERTICAL);
    }


    public void setText(String text) {
        this.text = text;
        checkBox = findViewById(R.id.checkBox);
        checkBox.setText(text);
        deleteButton = findViewById(R.id.deleteButton);
        setDeleteListener(deleteButton);
    }

    private void setDeleteListener(final ImageButton button) {
        button.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                checkBox.setPaintFlags(checkBox.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                checkBox.setEnabled(false);
                button.setEnabled(false);
            }
        });
    }

    private class SavedState extends BaseSavedState {

        public SavedState(Parcel source) {
            super(source);
        }

        public SavedState(Parcel source, ClassLoader loader) {
            super(source, loader);
        }

        public SavedState(Parcelable superState) {
            super(superState);
        }


    }

}
